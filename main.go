package main

import (
	"fmt" 
	"github.com/dchest/uniuri" // untuk generate random confirmation string di file deletion
	"io" // untuk return EOF (End of File) ketika program selesai membaca file.
	"os" // untuk file operation
)

var path = `C:\Users\Luthfi\OneDrive\Documents\Kuliah\Intern\cobacoba\basic-go-crud\user_log.txt` // path buat .txt file baru
var u User // supaya struct User bisa dibaca secara public & mempersingkat nama

type User struct {
	name string
	age  int
}

func isError(err error) bool {
	if err != nil {
		fmt.Println(err.Error())
	}
	return (err != nil)
}

func menu() {
	var opt int

	fmt.Println("[1] Add user to log")
	fmt.Println("[2] View all user information")
	fmt.Println("[3] Update user information")
	fmt.Println("[4] Delete user from log")
	fmt.Println("[5] Delete log")
	fmt.Println("[6] Quit")
	fmt.Printf(">> ")
	fmt.Scan(&opt)

	switch opt {
	case 1:
		addUser()
	case 2:
		viewFile()
	case 3:
		
	case 4:
		// deleteUser()
	case 5:
		deleteFile()
	case 6:
		fmt.Println("Good bye!")
	default:
		menu()
	}
}

func deleteFile() {
	s := uniuri.NewLen(6)
	var confirmationString string

	fmt.Println("Type the confirmation string below to confirm deletion or type 0 to cancel deletion")
	fmt.Println(s)
	fmt.Scan(&confirmationString)

	if confirmationString != s && confirmationString != `0` {
		println("Confirmation string is wrong")
		deleteFile()
	} else if confirmationString == `0` {
		fmt.Println("File deletion cancelled")
		menu()
	} else if confirmationString == s {
		os.Remove(path)
		fmt.Println("File deletion success!")
	}

}

func viewFile() {
	var file, err = os.OpenFile(path, os.O_RDONLY, 0644)
    if isError(err) { 
		return 
	}
    defer file.Close() 

    
    var text = make([]byte, 1024)
    for {
        n, err := file.Read(text)
        if err != io.EOF { // Kalo 
            if isError(err) {
				break 
			}
        }
        if n == 0 {
            break
        }
    }
    if isError(err) { 
		return 
	}

    fmt.Println("File contents : ")
    fmt.Println(string(text))

	menu()
}

func createFile() {

	var _, err = os.Stat(path)

	if os.IsNotExist(err) {
		var file, err = os.Create(path)
		if isError(err) {
			return
		}
		defer file.Close()
	}
	fmt.Println("File has been made", path)
}

func createUser() {

	fmt.Println("Enter your name and age to continue!")
	fmt.Printf("Enter your name : ")
	fmt.Sscan(u.name)

	fmt.Printf("Enter your age : ")
	fmt.Scan(&u.age)

	fmt.Println(u.name+",", u.age)
}

func addUser() {
	var file, err = os.OpenFile(path, os.O_RDWR|os.O_APPEND, 0644)
	if isError(err) {
		return
	}
	defer file.Close()

	_, err = fmt.Fprintf(file, u.name+",") // kenapa pake fprintf? supaya kita bisa mau ngeprint formatted strings 
	
	if isError(err) {
		return
	}

	_, err = fmt.Fprintf(file, " %d\n", u.age)

	if isError(err) {
		return
	}

	err = file.Sync()
	if isError(err) {
		return
	}

	fmt.Println("Entry added to log!")
	menu()
}

func main() {
	createFile()
	createUser()
	menu()
}
